# about

this contains information about tutorials for ML Autumn school.


!!! info "environment"

    pip packages used

    - git+https://gitlab.com/ase/ase@master
    - torch_ema
    - torch
    - torchaudio
    - torchvision
    - git+https://github.com/ACEsuit/mace@develop
    - xtb
    - rdkit
    - nvitop
    - git+https://github.com/imagdau/aseMolec.git@main
    - git+https://github.com/lab-cosmo/librascal.git@master
    - dscribe
    - opentsne
    - hdbscan
    - skmatter
    - pymatgen

    with extra url if you want to use cuda and nvidia gpus https://download.pytorch.org/whl/cu118

      mamba packages

    - altair
    - beautifulsoup4
    - bokeh
    - bottleneck
    - cloudpickle
    - conda-forge::blas=*=openblas
    - cython
    - dask
    - dill
    - h5py
    - ipympl
    - ipywidgets
    - matplotlib-base
    - numba
    - numexpr
    - pandas
    - patsy
    - protobuf
    - pytables
    - scikit-image
    - scikit-learn
    - scipy
    - seaborn
    - sqlalchemy
    - statsmodels
    - sympy
    - widgetsnbextension
    - jupyterlab_widgets
    - nglview
    - openmpi
    - hdf5
    - openmpi-mpicc
    - openmpi-mpicxx
    - openmpi-mpifort
    - scalapack
    - xlrd
    - spglib
